#ifndef ADDRESSSERVICE_H
#define ADDRESSSERVICE_H

#include <QObject>

#include <QGeoServiceProvider>
#include <QPlaceSearchReply>
#include <QGeoAddress>

#include <QVariant>

class AddressService : public QObject
{
  Q_OBJECT
public:
  explicit AddressService(QGeoServiceProvider * geoServiceProvider, QObject *parent = nullptr);

  void setBaseAddress(QString address, QVariant userData = QVariant());
  void checkAddress(QString address);

signals:
  void same_places(bool isSame, QVariant userData);
  void ready();

public slots:

protected:
  QGeoServiceProvider * geoServiceProvider;
  QPlaceSearchReply * baseReply, * otherReply;
  QPlace basePlace, otherPlace;
  bool isBasePlace, isOtherPlace;
  QVariant baseUserData;

  QPlaceSearchReply* getPlaceReply(QString address);

  bool handleSearchReaply(QPlaceSearchReply* otherReply, QPlace* place);
  void compare_places();

protected slots:
  void handleBaseSearchReply();
  void handleOtherSearchReply();
};

#endif // ADDRESSSERVICE_H
