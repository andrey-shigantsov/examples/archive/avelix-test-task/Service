#include "AddressService.h"

#include <QPlaceManager>
#include <QPlaceSearchRequest>
#include <QPlaceSearchReply>
#include <QPlaceSearchResult>
#include <QPlaceResult>

#include <QDebug>

AddressService::AddressService(QGeoServiceProvider* geoServiceProvider, QObject *parent) : QObject(parent)
{
  Q_ASSERT(geoServiceProvider);
  this->geoServiceProvider = geoServiceProvider;

  baseReply = otherReply = nullptr;
  isBasePlace = isOtherPlace = false;
}

void AddressService::setBaseAddress(QString address, QVariant userData)
{
  Q_ASSERT(!baseReply);
  isBasePlace = false;
  baseUserData = QVariant();
  baseReply = getPlaceReply(address);
  if (!baseReply) return;

  baseUserData = userData;
  connect(baseReply, &QPlaceSearchReply::finished, this, &AddressService::handleBaseSearchReply);
}

void AddressService::checkAddress(QString address)
{
  Q_ASSERT(!otherReply);
  isOtherPlace = false;
  otherReply = getPlaceReply(address);
  if (!otherReply) return;

  connect(otherReply, &QPlaceSearchReply::finished, this, &AddressService::handleOtherSearchReply);
}

QPlaceSearchReply * AddressService::getPlaceReply(QString address)
{
  QPlaceManager * manager = geoServiceProvider->placeManager();
  if (geoServiceProvider->error() != QGeoServiceProvider::NoError)
  {
    qWarning() << "AddressService: get Place Manager failure: " << geoServiceProvider->errorString();
    return nullptr;
  }

  QPlaceSearchRequest searchRequest;
  searchRequest.setSearchTerm(address);

  return manager->search(searchRequest);
}

bool AddressService::handleSearchReaply(QPlaceSearchReply* reply, QPlace * place)
{
  bool res = false;
  if (reply->error() == QPlaceReply::NoError)
  {
    foreach (const QPlaceSearchResult &result, reply->results())
    {
      if (result.type() == QPlaceSearchResult::PlaceResult)
      {
        QPlaceResult placeResult = result;
        *place = placeResult.place();
        qDebug() << "PlaceId: " << placeResult.place().placeId();
        qDebug() << "Country: " << placeResult.place().location().address().country();
        qDebug() << "County: " << placeResult.place().location().address().county();
        qDebug() << "State: " << placeResult.place().location().address().state();
        qDebug() << "City: " << placeResult.place().location().address().city();
        qDebug() << "District: " << placeResult.place().location().address().district();
        qDebug() << "Street: " << placeResult.place().location().address().street();
        qDebug() << "Postal Code: " << placeResult.place().location().address().postalCode();

        res = true;
        break; // TODO: add suppurt for multiple places
      }
    }
  }
  reply->deleteLater();
  return res;
}

void AddressService::compare_places()
{
  if (isBasePlace && isOtherPlace)
  {
    emit same_places(basePlace == otherPlace, baseUserData);
    emit ready();
  }
}

void AddressService::handleBaseSearchReply()
{
  qDebug() << "Handle base addreess search reaply...";
  disconnect(baseReply, &QPlaceSearchReply::finished, this, &AddressService::handleBaseSearchReply);

  isBasePlace = handleSearchReaply(baseReply, &basePlace);
  baseReply = nullptr;

  compare_places();
}

void AddressService::handleOtherSearchReply()
{
  qDebug() << "Handle other addreess search reaply...";
  disconnect(otherReply, &QPlaceSearchReply::finished, this, &AddressService::handleOtherSearchReply);

  isOtherPlace = handleSearchReaply(otherReply, &otherPlace);
  otherReply = nullptr;

  compare_places();
}

