#ifndef SERVICE_H
#define SERVICE_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QSqlDatabase>
#include <QGeoServiceProvider>

class Service : public QObject
{
  Q_OBJECT
public:
  explicit Service(QObject *parent = nullptr);

  void setPort(quint16 port){tcpPort = port;}

  void setDatabaseDriver(QString sqlDriver);
  void setDatabaseHost(QString host);
  void setDatabaseName(QString name);
  void setDatabaseUser(QString usr);
  void setDatabasePassword(QString pwd);
  bool openDatabase();

  void setGeoServiceProvider(QString provider);
  bool startGeoService();

  bool start();
  void stop();

signals:

public slots:

protected:
  QTcpServer tcpServer;
  quint16 tcpPort;

  QString dbDriver, dbHost, dbName, dbUser, dbPwd;
  QSqlDatabase db;

  QString geoServiceProviderName;
  QGeoServiceProvider * geoServiceProvider;

protected slots:
  void newClient();
};

#endif // SERVICE_H
