cmake_minimum_required (VERSION 2.8.11)

project (Service-TestTaskCpp-Avelix)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)

find_PACKAGE(Qt5Core CONFIG REQUIRED)
find_PACKAGE(Qt5Network CONFIG REQUIRED)
find_PACKAGE(Qt5Sql CONFIG REQUIRED)
find_PACKAGE(Qt5Location CONFIG REQUIRED)

set(SOURCES_DIR ${CMAKE_CURRENT_SOURCE_DIR}/src)

file(GLOB_RECURSE SOURCES
  ${SOURCES_DIR}/*.h
  ${SOURCES_DIR}/*.cpp
  ${SOURCES_DIR}/*.ui)
  
set(TARGET service.testtask.avelix)
add_executable(${TARGET} ${SOURCES})
target_link_libraries(${TARGET} Qt5::Core Qt5::Network Qt5::Sql Qt5::Location)
